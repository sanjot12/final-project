import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;
import java.lang.Math;
/**
 * Origonal code by Tim McGowen on 3/27/2018
 * Edited by Sanjot Chandi 5/08/2021
 * Model to get Cryptocurrency information based on acronym of the currency i.e. BTC for bitcoin.
 */
public class CdModel {
  private JsonElement jse;

  public boolean getCd(String symbol)
  {
    String key = "w3ggclepv7sfuijdg9402d";
    try
    {
      URL cdURL = new URL("https://api.lunarcrush.com/v2?data=assets&key="
					+ key
					+ "&symbol="
               +symbol);

      // Open connection
      InputStream is = cdURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
   }
   catch (java.io.UnsupportedEncodingException uee)
   {
     uee.printStackTrace();
   }
   catch (java.net.MalformedURLException mue)
   {
     mue.printStackTrace();
   }
   catch(java.io.FileNotFoundException fnf)
   {
      return false;
   }
   catch (java.io.IOException ioe)
   {
     //ioe.printStackTrace();
     return false;
   }
   catch (java.lang.NullPointerException npe)
   {
     npe.printStackTrace();
   }
    // Check to see if the symbol is valid.
   return isValid();
 }


  public String getName()
  {
    String name=jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString();
    //String state=jse.getAsJsonObject().get("sys").getAsJsonObject().get("country").getAsString();
    return name;//city and country
  }

  public double getPrice()
  {
     double price=jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("price").getAsDouble();
     //java.util.Date time=new java.util.Date((long)timeStamp*1000);
     return Math.round(price*100d)/100d;
  }

  public double getMarketCap()
  {
    return Math.round(jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("market_cap").getAsDouble()*100d)/100d;
  }

  public double getPercentchange24h()
  {
    return Math.round(jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("percent_change_24h").getAsDouble()*100d)/100d;
  }

  public double getPercentChange7d()
  {
    return(jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("percent_change_7d").getAsDouble());
  }

  public double getPercentChange30d()
  {
    //float pressure =(Math.round(0.02953*(jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsDouble())) * 1000) / 1000;
    return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("percent_change_30d").getAsDouble();
  }

  public double getVolume()
  {
    return jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("volume_24h").getAsDouble();
    //return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();
  }

 /* public double getPrediction()
  {
    return jse.getAsJsonObject().get(data).getAsArray().get(0).getAsObject().get("sentiment_absolute").getAsDouble();
    //return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();
  }*/  

  //public Image getImage()
  //{
    //String iconURL = jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("icon").getAsString();
    //String url="http://openweathermap.org/img/wn/"+iconURL+"@2x.png";
    //return new Image(url);
  //}
  public boolean isValid()
  {
    // If the zip is not valid we will get an error field in the JSON
    try 
    {
      System.out.println(jse.getAsJsonObject().get("error").getAsString());
      String error = jse.getAsJsonObject().get("error").getAsString();
      return false;
    }
//    catch (java.io.IOException ioee)
  //  {
   //   return false;
    
    catch (java.lang.NullPointerException npe)
    {
      // We did not see error so this is a valid zip
      //System.out.println(jse.getAsJsonObject().get("message").getAsString());
      return true;
    }
   }
    //turn meteorological directions(degress) to compass direction(N, E, S,W)
  public int getSocialImpactScore()
  {
     double scale = jse.getAsJsonObject().get("data").getAsJsonArray().get(0).getAsJsonObject().get("sentiment_absolute").getAsDouble();     
     int scaleInt=(int)scale;
     return(scaleInt);
  }
   public String convertNumToText(double num)
   {
      if (num<=999999)
      {   
         return num+"";
      }
      else if (num>999999 && num<=999999999)
      {   
         return Math.round((num/1000000)*100d)/100d+" Million";
      }
      else
      {
         return Math.round((num/1000000000)*100d)/100d+ " Billion";
      }
   
   
   }

}