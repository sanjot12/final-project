import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CdModelTest 
{

   @Test
   public void testGetCd1()
   {
      CdModel data = new CdModel();
      assertEquals(true, data.getCd("eth"));
   }
   
   @Test
   public void testGetCd2()
   {
      CdModel data = new CdModel();
      assertEquals(false, data.getCd("pog"));
   }
   
   @Test
   public void testGetSocialImpactScore()
   {
      CdModel data = new CdModel();
      data.getCd("btc");
      //Was going to validate that getSocialImpactScore returns double but that is built in into the return type of the function 
      assertTrue(0 <= data.getSocialImpactScore() && data.getSocialImpactScore() <= 5);


   }  
   
   @Test
   public void testGetName()
   {
      CdModel data = new CdModel();
      data.getCd("btc");
      assertEquals("Bitcoin",data.getName());


   }  
   
   @Test
   public void testconvertNumToText()//had to add additional test
   {
      CdModel data = new CdModel();
      data.getCd("btc");
      assertEquals("10.71 Million", data.convertNumToText(10710000));
      assertEquals("1.23 Billion", data.convertNumToText(1230000000));

   }   
   
   
}  
   
   
      