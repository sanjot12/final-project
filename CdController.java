/*
 * WxController
 * This is the controller for the FXML document that contains the view. 
 */
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Tim McGowen
 * @edited by Sanjot Chandi
 */
public class CdController implements Initializable {

  @FXML
  private Button btnGetCd;

  @FXML
  private TextField txtSymbol;

  @FXML
  private Label lblName;

  @FXML
  private Label lblPrice;

  @FXML
  private Label lblMarketCap;

  @FXML
  private Label lblPC24hr;

  @FXML
  private Label lblPC7d;

  @FXML
  private Label lblPC30d;

  @FXML
  private Label lblVolume;

  @FXML
  private ImageView iconPrediction;

  @FXML
  private void handleButtonAction(ActionEvent e) {
    // Create object to access the Model
    CdModel data = new CdModel();

    // Get zipcode
    String symbol = txtSymbol.getText();

    // Use the model to get the weather information
    if (data.getCd(symbol))
    {
      lblName.setText(data.getName());
      lblPrice.setText(String.valueOf("$"+data.getPrice()));
      lblMarketCap.setText("$"+data.convertNumToText(data.getMarketCap()));
      lblPC24hr.setText(String.valueOf(data.getPercentchange24h())+"%");
      lblPC7d.setText(String.valueOf(data.getPercentChange7d())+"%");
      lblPC30d.setText(String.valueOf(data.getPercentChange30d())+"%");
      lblVolume.setText("$"+data.convertNumToText(data.getVolume()));
      if (data.getSocialImpactScore()>3)
         iconPrediction.setImage(new Image("Bullish.png"));
      else
         iconPrediction.setImage(new Image("Bearish.png"));
    }
    else
    {
      lblName.setText("Invalid Symbol");
      lblPrice.setText("You Fool!!!");
      lblMarketCap.setText("The Moon is a lie");
      lblPC24hr.setText("");
      lblPC7d.setText("Buy Doge");
      lblPC30d.setText("");
      lblVolume.setText("");
      iconPrediction.setImage(new Image("badsymbol.png"));
    }
  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    // TODO
  }    

}
